require 'colorize'

class Game


  def initialize(guessing_player, checking_player)
    @guessing_player = guessing_player
    @checking_player = checking_player
  end

  def run
    masked_word = @checking_player.set_secret_word
    puts "Guess a letter: #{masked_word}".blue
    letter_guess = @guessing_player.make_guess(masked_word)
    masked_word = @checking_player.update_masked_word(masked_word, letter_guess)

    while masked_word.include?("_")
      puts "You've guessed: #{@guessing_player.guessed_letters.join(", ")}".blue
      puts "Guess a letter: #{masked_word}".blue
      letter_guess = @guessing_player.make_guess(masked_word)
      masked_word = @checking_player.update_masked_word(masked_word, letter_guess)
    end

    @guessing_player.guessed_letters = []
    puts masked_word
    puts "#{@guessing_player.name} won!"
  end

end

class HumanPlayer
  attr_reader :name
  attr_accessor :guessed_letters

  def initialize(name)
    @name = name
    @guessed_letters = []
  end

  def make_guess(masked_word)
    guess = gets.chomp
    @guessed_letters << guess
    guess
  end

  def set_secret_word
    puts "Think of a secret word".green
    puts "Enter how many letters it is:".green
    secret_word_length = gets.chomp.to_i

    "_" * secret_word_length
  end

  def update_masked_word(masked_word, letter_guess)
    puts "Computer guessed: #{letter_guess}".green
    puts "If that's in your word, where is its index or indices?".green

    indices = gets.chomp
    indices += "," if indices.length == 1
    indices = indices.split(",")

    indices.each do |index|
      masked_word[index.to_i] = letter_guess unless index == ""
    end

    masked_word
  end
end

class ComputerPlayer

  attr_reader :secret_word, :name
  attr_accessor :guessed_letters

  def initialize
    @name = "Johnny"
    @secret_word = ""
    @first_turn = true
    @dictionary = get_dictionary
    @guessed_letters = []
  end

  def set_secret_word
    @secret_word = File.readlines("edited_dictionary.txt").sample.chomp

    "_" * @secret_word.length
  end

  def get_dictionary
    dictionary = File.readlines("edited_dictionary.txt")
    dictionary.map!(&:chomp)
  end

  def make_guess(masked_word)

    ordered_letters = filter_words(masked_word)

    while @guessed_letters.include?(ordered_letters.first)
      ordered_letters.shift
    end

    @guessed_letters << ordered_letters.shift

    @guessed_letters.last
  end

  def filter_words(masked_word)
    words_of_right_length = @dictionary.select { |word| word.length == masked_word.length }
    words_with_correct_positions = get_words_with_correct_positions(words_of_right_length, masked_word)
    right_letters = words_with_correct_positions.join("").split("")
    order_letters(right_letters)
  end

  def get_words_with_correct_positions(words_of_right_length, masked_word)
    best_words = words_of_right_length
    masked_letters_array = masked_word.split("")

    masked_letters_array.each_with_index do |letter, letter_index|
      unless letter == "_"
        best_words = best_words.select { |word| word[letter_index] == letter }
      end
    end

    best_words
  end

  def order_letters(letters)
    letter_frequency = Hash.new(0)
    letters.each do |letter|
      letter_frequency[letter] += 1
    end

    # letter_frequency.sort! { |key, value| value }
    # ordered_letters = letter_frequency.keys

    ordered_letters = []
    until letter_frequency == {}
      ordered_letters << letter_frequency.key(letter_frequency.values.max)
      letter_frequency.delete(ordered_letters.last)
    end

    ordered_letters
  end

  def update_masked_word(masked_word, letter_guess)
    secret_letters_array = @secret_word.split("")
    index_replacement_array = []

    # checks to see if 'letter_guess' is in '@secret_word'
    if secret_letters_array.include?(letter_guess)
      secret_letters_array.each_with_index do |secret_letter, index|
        index_replacement_array << index if secret_letter == letter_guess
      end

      # replaces underscores with 'letter_guess'
      index_replacement_array.each do |index|
        masked_word[index] = letter_guess
      end
    end

    masked_word
  end
end

1000.times.do
  human = HumanPlayer.new("Mickey")
  comp = ComputerPlayer.new
  comp2 = ComputerPlayer.new
  # g = Game.new(human, comp)
  # g = Game.new(comp, human)
  g = Game.new(comp, comp2)
  g.run
end