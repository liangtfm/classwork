def my_transpose(matrix)
  new_matrix = []
  size = (matrix.length)
  numbers = matrix.flatten

  0.upto(size) do |line_number|
    new_row = []
    numbers.each_with_index do |num,idx|
      new_row << num if idx % size == line_number
    end
  new_matrix << new_row
  end

  new_matrix[0...size]
end

rows = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8]]

p "original: "
p rows
p "final: "
p my_transpose(rows)