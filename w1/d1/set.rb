def set_add_el(hash, new_key)
  hash[new_key] = true
  hash
end

def set_remove_el(hash,doomed_key)
  hash.delete(doomed_key)
  hash
end

def set_list_els(hash)
  hash.keys
end

def set_member?(hash, key)
  hash.has_key?(key)
end

def set_union(hash1, hash2)
  hash1.merge(hash2)
end

def set_intersection(hash1, hash2)

  new_hash = {}

  hash1.each do |key1, val1|
    hash2.each do |key2, val2|
      new_hash[key1] = val1 if key1 == key2
    end
  end

  new_hash
end

def set_minus (hash1, hash2)
  new_hash = {}

  hash1.each do |key1, val1|
    hash2.each do |key2, val2|
      new_hash[key1] = val1 if key1 != key2
    end
  end

  new_hash
end

p set_add_el({}, :x) # => make this return {:x => true}
p set_add_el({:x => true}, :x) # => {:x => true} # This should automatically work if the first method worked
p set_remove_el({:x => true}, :x) # => {}
p set_list_els({:x => true, :y => true}) # => [:x, :y]
p set_member?({:x => true}, :x) # => true
p set_union({:x => true}, {:y => true}) # => {:x => true, :y => true}
p set_intersection({:x => true,:y => true}, {:y => true}) # I'm not going to tell you how the last two work
p set_minus({:x => true,:y => true}, {:y => true}) # Return all elements of the first hash that are not in the second hash, not vice versa