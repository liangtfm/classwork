def caesar_cipher(phrase, shift)
  letters = phrase.downcase.split(//)
  letter_codes = letters.map(&:ord)

  shifted_codes = letter_codes.map do |code|
    code += shift unless code < "a".ord
    code -= 26 if code > "z".ord
    code
  end

  shifted_codes.map(&:chr).join
end

