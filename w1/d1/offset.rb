def correct_hash(wrong_hash)
  better_hash = {}

  wrong_hash.each do |key, value|
    new_key = key.to_s.next.to_sym
    better_hash[new_key] = value
  end

  better_hash
end

wrong_hash = { :a => "banana", :b => "cabbage", :c => "dental_floss", :d => "eel_sushi" }

p correct_hash(wrong_hash)