class Array
  def my_uniq
    unique_array = []
    self.each do |num|
      unique_array << num unless unique_array.include?(num)
    end
    unique_array
  end
end

p [1,2,3,1,2,3].my_uniq