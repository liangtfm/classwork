def num_to_s(num, base)
  return "0" if num == 0

  digits_key = [0,1,2,3,4,5,6,7,8,9,"a","b","c","d","e","f"]

  result_digits = []
  still_to_divide = num


  until still_to_divide == 0

    result_digits << still_to_divide % base
    still_to_divide /= base

  end

  converted_number = ""
  result_digits.reverse.each do |number|
    converted_number << digits_key[number].to_s
  end
  converted_number
end


#puts num_to_s(5, 10) #=> "5"
#puts num_to_s(5, 2)  #=> "101"
#puts num_to_s(5, 16) #=> "5"

#puts num_to_s(234, 10) #=> "234"
#puts num_to_s(234, 2)  #=> "11101010"
puts num_to_s(234, 16) #=> "EA"
puts num_to_s(0, 12)