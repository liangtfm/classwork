def my_concat(strings)
  result = strings.map.inject("") do |combined_string, string|
    combined_string += "#{string}"
  end

  result
end