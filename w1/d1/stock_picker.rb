def stock_picker(stock_prices)
  biggest_profit = 0
  buy_and_sell_days = []

  stock_prices.each_with_index do |buy_price, buy_day|
    buy_day.upto(stock_prices.length - 1) do |sell_day|
      profit = stock_prices[sell_day].to_i - buy_price.to_i
      if profit > biggest_profit
        buy_and_sell_days = [buy_day, sell_day]
        biggest_profit = profit
      end
    end
  end

  buy_and_sell_days
end

p stock_picker([2, 3, 4, 5, 10, 8, 7, 6])