class Fixnum
  def in_words
    return "zero" if self == 0

    number_string = []

    trillions_place = self / 1_000_000_000_000
    if trillions_place != 0
      number_string << "#{trillions_place.in_words} trillion"
    end
    remainder = self % 1_000_000_000_000

    billions_place = remainder / 1_000_000_000
    if billions_place != 0
      number_string << "#{billions_place.in_words} billion"
    end
    remainder = remainder % 1_000_000_000

    millions_place = remainder / 1_000_000
    if millions_place != 0
      number_string << "#{millions_place.in_words} million"
    end
    remainder = remainder % 1_000_000

    thousands_place = remainder / 1_000
    if thousands_place != 0
      number_string << "#{thousands_place.in_words} thousand"
    end
    remainder = remainder % 1_000

    hundreds_place = remainder/ 100
    if hundreds_place != 0
      number_string << "#{ones_place_in_words(hundreds_place)} hundred"
    end
    remainder = remainder % 100

    tens_place = remainder / 10
    if tens_place > 1
      number_string << "#{tens_place_in_words(tens_place)}"
    elsif tens_place == 1
      teen_number = remainder
      number_string << "#{teens_in_words(teen_number)}"
    end
    remainder = remainder % 10

    ones_place = remainder
    unless (tens_place == 1) || (ones_place == 0)
      number_string << "#{ones_place_in_words(ones_place)}"
    end

    return number_string.join(" ")

  end

  def ones_place_in_words(int)
    digit_names = %w{one two three four five six seven eight nine}
    return digit_names[(int - 1)]
  end

  def tens_place_in_words(int)
    tens_place_names = %w{ten twenty thirty
                          forty fifty sixty seventy eighty ninety}
    return tens_place_names[(int - 1)]
  end

  def teens_in_words(int)
    teen_names = %w{ten eleven twelve thirteen fourteen fifteen sixteen
                    seventeen eighteen nineteen}
    return teen_names[int % 10]
  end
end


# 599 = "five hundred ninety nine"