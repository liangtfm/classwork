def median_finder (integers)
  integers.sort!
  if integers.length.odd?
    median = integers[integers.length/2]
  else
    median = (integers[integers.length/2] + integers[(integers.length/2) - 1])/2.0
  end
  median
end

p median_finder([2, 4, 6, 8,1])