require 'colorize'

class Maze
  attr_accessor :down, :over

  def initialize(down, over)
    @down = down
    @over = over
  end
    @@maze_graph = %q{****************
*         *   E*
*    *    *  ***
*    *    *    *
*    *    *    *
*    *    *    *
*    *         *
****************}.split("\n").map{|line| line.split("")}

  def self.graph
    @@maze_graph
  end

  def starting_position
    @@maze_graph[@down][@over] = "S"
    display
  end

  def display
    @@maze_graph.each do |line| puts line.join end
  end

  def east
    until @@maze_graph[@down][@over + 1] == ("*")
      @@maze_graph[@down][@over + 1] = "x".red
      @over += 1
    end
  end

  def west
    until @@maze_graph[@down][@over - 1] == ("*")
      @@maze_graph[@down][@over - 1] = "x".red
      @over -= 1
    end
  end

  def south
    until @@maze_graph[@down + 1][@over] == ("*")
      @@maze_graph[@down + 1][@over] = "x".red
      @down += 1
    end
  end

  def north
    until @@maze_graph[@down - 1][@over] == ("*")
      @@maze_graph[@down - 1][@over] = "x".red
      @down -= 1
    end
  end

  def explorex
    until @@maze_graph[1][13] == "E"
      if @@maze_graph[@down][@over + 1] == ""
        east
      elsif @@maze_graph[@down - 1][@over] == ""
        north
      elsif @@maze_graph[@down + 1][@over] == ""
        south
      else
        west
      end
    end

  end

  def explore
      east if @@maze_graph[@down][@over + 1] == " "
      north if @@maze_graph[@down - 1][@over] == " "
  end

end

# Create a new Maze object with 'down' and 'over' coordinates
p maze = Maze.new(6, 1)
maze.starting_position
maze.explore
maze.display