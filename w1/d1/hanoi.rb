class HanoiGame
  attr_reader :disks, :towers

  def initialize(disks=3)
    @disks = disks
    @towers = create_towers(disks)
  end

  def create_towers(number_of_disks)
    towers = Array.new(3) {Array.new(number_of_disks) {nil}}
    towers[0] = (1..number_of_disks).to_a
    towers
  end

  def draw
    puts "T1\tT2\tT3"
    puts
    draw_towers = @towers.transpose.reverse
    draw_towers.map { |tower| puts tower.join("\t")}
  end

  def move_disk(from, to)
    disk = @towers[from-1].shift
    @towers[from-1].push(nil)

    return "There's no disk there!" if disk.nil?
    if @towers[to-1][0].nil?
      @towers[to-1] = trim(@towers[to-1].unshift(disk))
      return draw
    elsif @towers[to-1][0] > disk
      @towers[to-1] = trim(@towers[to-1].unshift(disk))
      return draw
    else
      @towers[from-1] = trim(@towers[from-1].unshift(disk))
      return "Piece cannot be moved there."
    end
  end

  def trim(arr)
    arr.take(@disks)
  end
end