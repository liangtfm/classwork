def range(start,finish)
  arr = [start]

  return arr if start == finish

  arr << range(start+1,finish)

  arr.flatten
end

def sum_recursive(arr)
  return arr[0] if arr.count == 1
  sum = arr.shift + sum_recursive(arr)
  sum
end

def sum_iterative(arr)
  arr.inject(:+)
end

def exp1(b, n)
  return 1 if n == 0
  b * exp1(b, n - 1)
end

def exp2(b, n)
  return 1 if n == 0
  if n.even?
    exp2(b, n/2) * exp2(b, n/2)
  else
    b * (exp2(b, (n - 1) / 2) * exp2(b, (n - 1) / 2))
  end
end

def deep_dup(el)
  return el unless el.is_a?(Array)
  arr = []
  el.each do |sub_el|
    arr << deep_dup(sub_el)
  end

  arr
end

def fibonacci(n)
  arr = []
  (0...n).each do |number|
    arr << get_fib(number)
  end
  arr
end

def get_fib(n)
  return 0 if n == 0
  return 1 if n == 1
  return get_fib(n-1) + get_fib(n-2)
end

def binary_search(array, target)
  return nil unless array.include?(target)

  middle = (array.length-1)/2
  return middle if target == array[middle]

  if target < array[middle]
    return middle - (middle - binary_search(array[0...middle], target))
  else
    return middle + 1 + binary_search(array[middle+1...array.length], target)
  end

end

def make_change(amount, coins = [25, 10, 5, 1])
  arr = get_change_array(amount,coins)
  check_for_length = get_change_array(amount,coins[1...coins.length])
  arr.length <= check_for_length.length ? arr : check_for_length
end

def get_change_array(amount, coins = [25, 10, 5, 1])
  coins_copy = coins.dup
  if coins.include?(amount)
    return [amount]
  end
  change = []
  until amount >= coins[0] do
    coins.shift
  end
  change += [coins[0]]
  change += get_change_array(amount - coins[0], coins_copy)
  change
end

def merge_sort(arr)
  return arr if arr.count == 1
  middle = arr.count / 2
  left, right = arr[0...middle], arr[middle..-1]
  merge(merge_sort(left), merge_sort(right))
end

def merge(left, right)
  sorted = []

  until (left.empty? || right.empty?)
    if left.first <= right.first
      sorted << left.shift
    else
      sorted << right.shift
    end
  end
  left.empty? ? sorted += merge_sort(right) : sorted += merge_sort(left)

  sorted
end

def subsets(arr)
  return [arr] if arr.empty?
  prev_subsets = subsets(arr.take(arr.length - 1))
  prev_subsets + prev_subsets.map { |subset| subset + [arr.last] }
end

def subsets_iterative(arr)
  new_arr = []
  new_arr << []

  (0...arr.length).each do |i|
    new_arr << [arr[i]]
    (i+1...arr.length).each do |j|
      new_arr << arr[i..j]
    end
  end

  new_arr
end

