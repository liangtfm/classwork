class Array

  def my_each(&block)
    self.count.times do |i|
      block.call(self[i])
    end
  end

  def my_map(&block)
    arr = []
    self.my_each do |x|
      arr << block.call(x)
    end

    arr
  end

  def my_select(&block)
    arr = []
    self.my_each do |x|
      arr << x if block.call(x)
    end

    arr
  end

  def my_inject(&block)
    total = self[0]
    if block.call(total,self[0]) == (total * self[0])
      total = 1
    elsif
      block.call(total,self[0]) == (total / self[0])
      total = 1
    else
      self.delete(self[0]) if self[0] == 0
      total = 0
    end
    self.my_each do |x|
      total = block.call(total, x)
    end

    total
  end

  def my_sort!(&block)

    self.length.times do
      self.each_index do |i|
        if block.call(self[i], self[i + 1]) == -1
          next
        elsif block.(self[i], self[i + 1]) == 1
          self[i], self[i + 1] = self[i + 1], self[i]
        end
      end
    end

    self
  end

end

def eval_block(*args, &block)
  unless block_given?
    puts "No block given."
    return
  end

  arr = []
  args.each do |arg|
    arr << block.call(arg)
  end

  arr
end