DICTIONARY = File.readlines("dictionary.txt").map(&:chomp)

def adjacent_words(word, dictionary = DICTIONARY)
  dictionary.select { |w| one_letter_difference?(word, w) }
end

def one_letter_difference?(word1, word2)
  return false unless word1.length == word2.length

  different_letters = 0
  (0...word1.length).each do |i|
    different_letters += 1 unless word1[i] == word2[i]
  end

  return different_letters == 1
end

def explore_words(source, dictionary = DICTIONARY)
  words_to_expand = [source]
  candidate_words = dictionary.select { |word| word.length == source.length }
  all_reachable_words = [source]

  until words_to_expand.empty?
    adjacent_words(words_to_expand.pop, candidate_words).each do |word|
      candidate_words.delete(word)
      words_to_expand << word
      all_reachable_words << word
    end
  end

  all_reachable_words
end

def make_path(parents, source, target)
  path = []
  path << target

  until parents[target] == source
    path << parents[target]
    target = parents[target]
  end

  path << source

  path.reverse
end

def find_chain(source, target, dictionary = DICTIONARY)
  time1 = Time.new

  raise "Words aren't the same length!" unless source.length == target.length
  words_to_expand = [source]
  candidate_words = dictionary.select { |word| word.length == source.length }
  parents = {}

  until words_to_expand.empty?
    word1 = words_to_expand.shift
    break if word1 == target

    adjacent_words(word1, candidate_words).each do |word2|
      candidate_words.delete(word2)
      words_to_expand << word2
      parents[word2] = word1
    end
  end

  path = make_path(parents, source, target)
  time2 = Time.new

  p path
  puts "Took #{time2 - time1} seconds"
end