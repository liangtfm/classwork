class Board

  def initialize(n)
    @n = n
    @board = Array.new(n) {Array.new(n) {nil} }
  end

  def [](pos)
    x, y = pos[0], pos[1]
    @board[x][y]
  end

  def []=(pos, mark = :Q)
    x, y = pos[0], pos[1]
    @board[x][y] = mark
  end

  def has_conflict?(pos)
    x, y = pos[0], pos[1]

    row_has_conflict?(x) || col_has_conflict?(y) || diag_has_conflict?(x,y)
  end

  def row_has_conflict?(x)
    @board[x].include?(:Q)
  end

  def col_has_conflict?(y)
    new_board = @board.transpose
    new_board[y].include?(:Q)
  end

  def diag_has_conflict?(x,y)
    # until oob?(x) or oob?(y)
    # use block to do this nicely
    # up_left = block.call(-1,-1)
    # up_right = block.call(-1,1)
    # down_left = block.call(1,-1)
    # down_right = block.call(1,1)
    # up_left || up_right || down_left || down_right
  end

  def out_of_bounds?(num)
    num < 0 || num > (@n - 1)
  end
end