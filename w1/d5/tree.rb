class TreeNode
  attr_accessor :parent, :children, :value

  def initialize(value)
    @parent = nil
    @children = []
    @value = value
  end

  def add_child(child_node_value)
    child_node = TreeNode.new(child_node_value)
    child_node.parent = self
    @children << child_node
    child_node
  end

  def remove_child(child_node)
    # if child_node has multiple children
    unless child_node.children.empty?
      #   iterate through child_node.children
      child_node.children.each do |child|
        child.remove_child(child)
      end
    end
    @children.delete(child_node)
    child_node.parent = nil
    child_node.children = []

    self
  end

  def deep_dup(el)
    return el unless el.is_a?(Array)
    arr = []
    el.each do |sub_el|
      arr << deep_dup(sub_el)
    end

    arr
  end

  def dfs(value)
    p "Checking if #{self.value} is #{value}"
    return self if self.value == value

    self.children.each do |child|
      result = child.dfs(value)
      return result if result
    end

    puts "not found"
    nil
  end

  def bfs(value)
    return self if self.value == value

    queue = [self]

    until queue.empty?
      node = queue.shift
      return node if node.value == value
      queue += node.children
    end

    nil
  end
end


