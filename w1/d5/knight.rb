class KnightPathFinder

  attr_accessor :current_position, :board

  def initialize(starting_position)
    @current_position = starting_position
    @board = Board.new(8)
    @board[starting_position] = 'x'
    @move_tree = {}
  end

  def find_path(ending_position)

  end

  # def parse_position(pos)
  #   x, y = pos[0], pos[1]
  #   @board[x][y]
  # end

  def new_move_positions(pos)
    #finds all possible next steps
    x, y = pos[1], pos[0]
    viable_positions = []
    calculated_positions = [
      [y+2, x+1],
      [y-2, x+1],
      [y+1, x+2],
      [y-1, x+2],
      [y+2, x-1],
      [y-2, x-1],
      [y+1, x-2],
      [y-1, x-2]
    ]
    calculated_positions.each do |p|
      viable_positions << p if
        valid_move?(pos, p) &&
        !@board.out_of_bounds?(p)
    end
    viable_positions
  end

  def build_move_tree(final_position) # [1,2]

    next_moves = self.new_move_positions(@current_position)

    # 01/03/2013
    # TRYING TO BUILD MOVE TREE TO TAKE PARENTS AND CHILDREN
    # SOMETHING ABOUT @CURRENT_POSITION & NEW_MOVE_POSITIONS AS
    # CHILDREN

    # return move_tree when we find final_position

    until next_moves.empty?
      next_move = next_moves.shift
      #@move_tree[next_move] = @current_position
      p @move_tree
      return next_move if next_move == final_position
      next_moves += self.new_move_positions(next_move)

    end
    nil
  end

  def valid_move?(prev_pos, next_pos) # [x+1, y+2] => y, x
    x, y = next_pos[1], next_pos[0]
    c_x, c_y = prev_pos[1],prev_pos[0]
    return true if (((x - c_x).abs == 2 && (y - c_y).abs == 1) ||
                   ((x - c_x).abs == 1 && (y - c_y).abs == 2)) #&&
                   #@board[next_pos].nil?)
    false
  end

end


class Board

  attr_reader :board, :n

  def initialize(n)
    @n = n
    @board = Array.new(n) {Array.new(n) {nil} }
  end

  def [](pos)
      x, y = pos[0], pos[1]
      @board[x][y]
  end

  def []=(pos, mark = :Q)
    x, y = pos[0], pos[1]
    @board[x][y] = mark
  end

  def place_knight(pos)
    x, y = pos[0], pos[1]
    self[[x,y]] = :Q
  end

  def place_nil(pos)
    x, y = pos[0], pos[1]
    self[[x,y]] = nil
  end


  def out_of_bounds?(pos)
    x, y = pos[0], pos[1]
    x < 0 || x > (@n - 1) || y < 0 || y > (@n - 1)
  end

  def clear
    (0...@n).each do |i|
      (0...@n).each do |j|
        self[[i,j]] = nil
      end
    end
  end

end