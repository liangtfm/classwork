def remix(arr)
  remixed = []

  separated = separate(arr)
  mixed = mix(separated)
  
  (0..mixed.size).each do |i|
    remixed << [mixed[0][i], mixed[1][i]]
  end
    
  p remixed
end

def separate(arr)
  
  alcohol = []
  mixers = []
  
  arr.each do |i|
    mixers << i.pop
    alcohol << i.pop
  end
  
  [alcohol, mixers]
end

def mix(arr)
  
  alcohol = arr[0].shuffle
  mixers = arr[1].shuffle
  
  [alcohol, mixers]
end


remix([
  ["rum", "coke"],
  ["gin", "tonic"],
  ["scotch", "soda"]
])