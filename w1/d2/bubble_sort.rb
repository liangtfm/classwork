def bubble_sort(arr)
  
  sorted_array = arr.sort!
  
  until arr == sorted_array
    arr.each_index do |i|
    
      if arr[i] > arr[i+1]
        arr[i], arr[i+1] = arr[i+1], arr[i]
      end
      
    end
  end
  p arr
end