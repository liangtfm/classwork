class RPN  
  attr_accessor :stack
  
  def initialize
    @stack = []
  end
  
  def file_read(file)
    @stack = File.read(file)
    @stack = @stack.split.map { |el| el.to_i == 0 ? el.to_sym : el.to_i}
    self.calculate
  end
  
  def calculate
    case 
    when @stack[-1] == :+
      @stack.delete_at(-1)
      self.plus
    when   @stack[-1] == :-
        @stack.delete_at(-1)
        self.minus
    when   @stack[-1] == :*
        @stack.delete_at(-1)
        self.times
    when   @stack[-1] == :/
        @stack.delete_at(-1)
        self.divide            
    end
  end
  
  def value
    return @stack.last
  end
  
  def push(num)
    @stack << num
  end

  def plus
  if @stack.size == 0
     raise "calculator is empty"
   else
     i = 0
     result = @stack[-2]

     while i < 1
        result += @stack.last
        @stack.pop(2)
        i += 1
      end
    end

    @stack << result
  end


  def minus
   if @stack.size == 0
     raise "calculator is empty"
   else
     i = 0
     result = @stack[-2]

     while i < 1
       result -= @stack.last
       i += 1
       @stack.pop(2)
     end
   end

   @stack << result
  end


  def divide
   if @stack.size == 0
     raise "calculator is empty"
   else
     i = 0
     result = @stack[-2]

     while i < 1
       result = result.to_f / @stack.last.to_f
       i += 1
       @stack.pop(2)
     end
   end

   @stack << result
  end


  def times
   if @stack.size == 0
     raise "calculator is empty"
   else
     i = 0
     result = @stack[-2]

     while i < 1
       result = result.to_f * @stack.last.to_f
       i += 1
       @stack.pop(2)
     end
   end
   @stack << result
  end
  
end

if __FILE__ == $PROGRAM_NAME
  a = RPN.new
  puts "What file would you like to read?"
  file = gets.chomp
  a.file_read(file)
  p a.value
else
  a = RPN.new
  a.file_read(ARGV.first)
  p a.value
end

