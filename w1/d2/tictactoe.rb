class Board
  attr_accessor :board, :winning_mark
  
  def initialize
    @board = Array.new(3){Array.new(3){nil}}
    @winning_mark = nil
  end
  
  def won?
    return true if self.check_rows?
    return true if self.check_cols?
    return true if self.check_diag?
    return false  
  end
  
  def winner
    puts "#{@winning_mark} wins!"
  end
  
  def empty?(pos)
    down = pos.first
    over = pos.last
    @board[down][over].nil?
  end
  
  def place_mark(pos, mark)
    down = pos.first
    over = pos.last
    @board[down][over] = mark
  end
  
  def display
    @board.each do |line| 
      puts line.join 
    end
  end
  
  def check_rows?
    @board.each do |row|
      unless row.include?(nil)
        if row[0] == row[1] && row[1] == row[2]
          @winning_mark = row[0]
          return true 
        end
      end
    end
    return false
  end
  
  
  def check_cols?
    @board = @board.transpose
    state = self.check_rows?
    @board = @board.transpose
    state
  end
  
  
  def check_diag?
    return false if @board[1][1].nil?
    if @board[0][0] == @board[1][1] && @board[1][1] == @board[2][2]
      @winning_mark = @board[1][1]
      return true
    elsif @board[0][2] == @board[1][1] && @board[1][1] == @board[2][0]
      @winning_mark = @board[1][1]
      return true
    end  
  end
  
end


class Game
  
  def initialize
    @game_board = Board.new
    @player1 = HumanPlayer.new("Tim", "x")
    @player2 = HumanPlayer.new("Ant", "o")
  end
  
  def play
    
    @game_board.display
    
    until @game_board.won? == true
      puts "#{@player1.name}, where do you want to place your mark?"
      pos = gets.chomp.split(",").map {|x| x.to_i}      
      @game_board.place_mark(pos, @player1.mark)
      
      puts "Current board"
      @game_board.display
      
      break if @game_board.won? 
      
      puts "#{@player2.name}, where do you want to place your mark?"
      pos = gets.chomp.split(",").map {|x| x.to_i}      
      @game_board.place_mark(pos, @player2.mark)
      
      puts "Current board"
      @game_board.display
    end
    
    @game_board.winner
  end
end


class HumanPlayer
  attr_accessor :mark, :name
  
  def initialize(name, mark)
    @mark = mark
    @name = name
  end
  
end


class ComputerPlayer < HumanPlayer
  
  
  
end