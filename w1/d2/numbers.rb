class Game
  attr_accessor :computer_number, :guess_times
  
  def initialize
    @computer_number = rand(1..100)
    @guess_times = 0
  end
  
  def display
    @computer_number
  end
  
  def play
    
    guess = 0
    
    until guess == @computer_number
      puts "What is your guess?"
      guess = gets.chomp.to_i
      @guess_times += 1
      unless guess == @computer_number
        puts guess_check(guess.to_i)
      end
    end
    
    p "You win!"
    p @guess_times
  end
  
  def guess_check(guess)
      (guess > @computer_number) ? "too high" : "too low"
  end
  
end