def factors(num)
  
  factors = []
  i = 1
  
  while i < num
    if num % i == 0
      factors << i
    end
    i += 1
  end
  
  factors.uniq
end