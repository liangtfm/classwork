LOSING = [["rock", "scissors"],
           ["scissors", "paper"],
           ["paper", "rock"]]

OPTIONS = ["rock","paper","scissors"]

def rps(user_option)
  
  result = ""
  
  computer_option = OPTIONS.sample(1)
  computer_option << user_option
  
  if computer_option[0] == computer_option[1]
    result += "#{computer_option[0]}, draw"
  elsif LOSING.include?(computer_option)
    result += "#{computer_option[0]}, lose"
  else
    result += "#{computer_option[0]}, win"
  end
    
  result
end