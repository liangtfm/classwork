class Student
  
  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end
  
  def name
    "#{@first_name} #{@last_name}"
  end
  
  def courses
    @courses
  end
  
  def enroll(potential_course)
    if potential_course.students.include?(self.name)
      "You're already enrolled in this course."
    elsif self.has_conflict?(potential_course)
      "There's a schedule conflict."
    else
      "You're enrolled!"
      @courses << potential_course
      potential_course.students << self.name
    end
  end
  
  def has_conflict?(course)
    self.courses.each do |course1|
      if course.conflicts_with?(course1)
        return true
      end
    end
    return false
  end
  
  def course_load
    course_load = Hash.new(0)
    
    @courses.each do |course|
      course_load[course.department] += course.credits
    end
    
    course_load
  end
  
end


class Course
  attr_accessor :course_name, :department, :credits, :block, :days_of_week
  
  def initialize(course_name, department, credits, block, days_of_week)
    @course_name = course_name
    @department = department
    @credits = credits
    @students = []
    @block = block
    @days_of_week = days_of_week #array of days of week
  end    
  
  def students
    @students
  end
  
  def add_student(student)
    @students << student.name
  end
  
  def conflicts_with?(course)
    self.days_of_week.each do |day|
      if course.days_of_week.include?(day)
        if course.block == self.block
          return true
        end
      end
      
      return false
    end
  end
  
end