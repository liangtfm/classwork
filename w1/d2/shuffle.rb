class Shuffler
  
  def initialize
    @new_lines = []
  end
  
  def run
    file_name = get_input
    shuffle(file_name)
    save_shuffled(file_name)
  end
  
  def get_input
    puts "Which file do you want to shuffle?"
    gets.chomp
  end

  def shuffle(file_name)
    File.foreach("#{file_name}") do |line|
      @new_lines << line
    end
  
    @new_lines.shuffle!
  end

  def save_shuffled(file_name)
    File.open("#{file_name}-shuffled.txt", "w") do |text|
      text << @new_lines.join("\n")
    end
  end
end

s = Shuffler.new
s.run
